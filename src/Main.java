import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        File file = Paths.get(".", "src", "file.txt").toFile();

        ObjectOutputStream outputStream = new ObjectOutputStream(
                new FileOutputStream(file));

        OldCustomer oldCustomer1 = new OldCustomer();
        oldCustomer1.setName("Tom");
        oldCustomer1.setAge("30");
        oldCustomer1.setAddress("GoldStreet,30,20021,New York");

        OldCustomer oldCustomer2 = new OldCustomer();
        oldCustomer2.setName("Bob");
        oldCustomer2.setAge("25");
        oldCustomer2.setAddress("newStreet,70,20024,New York");

        Customer customer1 = new IndividualCustomerAdapter(oldCustomer1);
        Customer customer2 = new EntityCustomerAdapter(oldCustomer2);

        List<Customer> list = new ArrayList<>();

        list.add(customer1);
        list.add(customer2);

        outputStream.writeObject(list);

        ObjectInputStream inputStream = new ObjectInputStream(
                new FileInputStream(file));

        List<Customer> desList = (List<Customer>) inputStream.readObject();
        desList.forEach(System.out::println);


    }
}
