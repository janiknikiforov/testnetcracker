import java.io.Serializable;

public interface Customer extends Serializable{
    public String getName();
    public Address getAddress();
}
